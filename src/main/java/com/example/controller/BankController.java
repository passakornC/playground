package com.example.controller;

import com.example.domain.BankAccountRequest;
import com.example.domain.Person;
import com.example.entity.PersonEntity;
import com.example.service.BankService;
import com.example.util.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class BankController {

    @Autowired
    BankService bankService;

    @PostMapping(path = "/camp3/api/openAccount", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> openAccount(@RequestBody BankAccountRequest bankAccountRequest) {
        PersonEntity person = bankService.openAccount(bankAccountRequest.getAccountNo(), bankAccountRequest.getAccountType(), bankAccountRequest.getPersonId(), bankAccountRequest.getPersonFullName(), bankAccountRequest.getAmount());
        List<PersonEntity> people = Arrays.asList(person);
        return new ResponseEntity<Person>(DtoConverter.getPersonDto(people), HttpStatus.OK);
    }
}
