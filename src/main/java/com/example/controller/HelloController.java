package com.example.controller;

import com.example.config.CodeCampConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    private CodeCampConfig codeCampConfig;

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World";
    }


    @RequestMapping("/lab3")
    @ResponseBody
    String lab3() {
        return codeCampConfig.getLanguage();
    }
}

