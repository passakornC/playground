package com.example.util;

import com.example.domain.AccountType;
import com.example.domain.BankAccount;
import com.example.domain.Person;
import com.example.entity.PersonEntity;

import java.util.ArrayList;
import java.util.List;

public class DtoConverter {
    public static Person getPersonDto(List<PersonEntity> people) {
        Person personDto = new Person();
        personDto.setActive(people.get(0).getStatus().equals(1) ? true : false);
        personDto.setPersonId(people.get(0).getPersonId());
        personDto.setPersonFullName(people.get(0).getFullName());
        List<BankAccount> bankAccounts = new ArrayList<>();
        for (PersonEntity person : people) {
            BankAccount bankAccount = new BankAccount();
            bankAccount.setAccountNo(person.getBankAccount().getAccountNo());
            bankAccount.setAccountType(AccountType.valueOf(person.getBankAccount().getAccountType()));
            bankAccount.setActive(person.getBankAccount().getStatus().equals(1) ? true : false);
            bankAccount.setBalance(person.getBankAccount().getBalance());
            bankAccounts.add(bankAccount);
        }
        personDto.setAccounts(bankAccounts);
        return personDto;
    }
}
