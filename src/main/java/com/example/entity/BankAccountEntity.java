package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "bank_account")
public class BankAccountEntity {
    @Id
    @Column(name = "account_no", unique = true)
    private String accountNo;
    @Column(name = "account_type")
    private String accountType;
    @Column(name = "balance")
    private BigDecimal balance;
    @Column(name = "status")
    private Integer status;

    public BankAccountEntity() {
    }

    public BankAccountEntity(String accountNo, String accountType, BigDecimal balance, Integer status) {
        this.accountNo = accountNo;
        this.accountType = accountType;
        this.balance = balance;
        this.status = status;
    }

    public String getAccountNo() {
        return accountNo;
    }
    public BigDecimal getBalance() {
        return balance;
    }

    public Integer getStatus() {
        return status;
    }
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getAccountType() { return accountType; }
    public void setAccountType(String accountType) { this.accountType = accountType; }



}
