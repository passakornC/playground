package com.example.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "person")
@IdClass(PersonId.class)
public class PersonEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "person_id", unique = true)
    private String personId;

    @Id
    @Column(name = "account_no")
    private String accountNo;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = BankAccountEntity.class)
    @JoinColumn(name = "account_no", insertable = false, updatable = false)
    private BankAccountEntity bankAccount;

    @Column(name = "full_name")
    private String fullName;
    @Column(name = "status")
    private Integer status;

    public PersonEntity() {
    }

    public PersonEntity(String personId, BankAccountEntity bankAccount, String fullName, Integer status) {
        this.personId = personId;
        this.bankAccount = bankAccount;
        this.accountNo = bankAccount.getAccountNo();
        this.fullName = fullName;
        this.status = status;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public BankAccountEntity getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccountEntity bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }




}
