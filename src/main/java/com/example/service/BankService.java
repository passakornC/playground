package com.example.service;

import com.example.dao.BankDao;
import com.example.entity.PersonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class BankService {
    @Autowired
    private BankDao bankDao;

    public PersonEntity openAccount(String accountNo, String accountType, String personId, String personFullName, BigDecimal openingBalance) {
        return bankDao.openAccount(accountNo, accountType, personId, personFullName, openingBalance);
    }
}
