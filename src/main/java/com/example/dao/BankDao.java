package com.example.dao;

import com.example.entity.BankAccountEntity;
import com.example.entity.PersonEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class BankDao {
    @Autowired
    private PersonRepository personRepository;

    public final static Logger logger = LogManager.getLogger(BankDao.class);

    public PersonEntity openAccount(String accountNo, String accountType, String personId, String personFullName, BigDecimal openingBalance) {
        BankAccountEntity bankAccount = new BankAccountEntity(accountNo, accountType, openingBalance, 1);
        PersonEntity person = new PersonEntity(personId, bankAccount, personFullName, 1);
        personRepository.save(person);
        return person;
    }

}
