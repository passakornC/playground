package com.example.dao;

import com.example.entity.PersonEntity;
import com.example.entity.PersonId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<PersonEntity, PersonId> {
}
