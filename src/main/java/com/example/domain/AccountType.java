package com.example.domain;

public enum AccountType {
    SAVING,
    CURRENT
}
